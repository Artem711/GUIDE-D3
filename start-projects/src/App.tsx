// PLUGINS IMPORTS //
import React from "react"

// COMPONENTS IMPORTS //
import BarChartFood from "./pages/BarChartFood"
import PieChartBudget from "./pages/PieChartBudget"
import LineGraphFitness from "./pages/LineGraphFitness"
import CirclePackingGraph from "./pages/CirclePackingGraph"
import TreeDiagram from "./pages/TreeDiagram"

import LineGraph_Weather from "./pages/LineGraph_Weather"

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const App = () => {
  return (
    <div>
      {/* <BarChartFood /> */}
      {/* <PieChartBudget /> */}
      {/* <LineGraphFitness /> */}
      {/* <CirclePackingGraph /> */}
      {/* <TreeDiagram /> */}
      <LineGraph_Weather />
    </div>
  )
}

export default App
