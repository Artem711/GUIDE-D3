import firebase from "firebase/app"
import "firebase/firestore"

var FirebaseConfig = {
  apiKey: "AIzaSyClUCLPl4c4DDG-xzGEATJTe3EdfIQZXj8",
  authDomain: "social-app-d00ec.firebaseapp.com",
  databaseURL: "https://social-app-d00ec.firebaseio.com",
  projectId: "social-app-d00ec",
  storageBucket: "social-app-d00ec.appspot.com",
  messagingSenderId: "1076003258037",
  appId: "1:1076003258037:web:a567eefafacecc06f988fc",
  measurementId: "G-6TYNDYX9C2",
}

// Initialize Firebase
if (firebase.apps.length === 0) {
  firebase.initializeApp(FirebaseConfig)
}

const settings = { timestampsInSnapshots: true }
const db = firebase.firestore()
db.settings(settings as any)
export { db }

export default firebase
