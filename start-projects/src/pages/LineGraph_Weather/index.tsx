// PLUGINS IMPORTS //
import React, { useEffect } from "react"
import * as d3 from "d3"

// COMPONENTS IMPORTS //
import data from "./data"

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const LineGraphFitness = () => {
  useEffect(() => {
    renderGraph()
  }, [])

  const SIZE = 800

  const margin = { top: 40, right: 20, bottom: 50, left: 100 }
  const dimensions = {
    graphHeight: SIZE - margin.top - margin.bottom,
    graphWidth: SIZE - margin.left - margin.right,
  }

  function renderGraph() {
    const svg = d3.select("svg")

    const GGraph = svg
      .append("g")
      .attr("width", dimensions.graphWidth)
      .attr("height", dimensions.graphHeight)
      .attr("transform", `translate(${margin.right}, ${margin.top})`)

    // # Scales
    const x = d3.scaleTime().range([0, dimensions.graphWidth])
    const y = d3.scaleLinear().range([dimensions.graphHeight, 0])

    // # Axis groups
    const GxAxis = GGraph.append("g").attr(
      "transform",
      `translate(0, ${dimensions.graphHeight})`
    )
    const GyAxis = GGraph.append("g")

    // # D3 Line path generator
    const line = d3
      .line()
      .x(function (d: any) {
        return x(new Date(d.date))
      })
      .y(function (d: any) {
        return y(d.temp)
      })

    // append line path
    const path = GGraph.append("path")
  }
  return <svg height={SIZE} width={SIZE}></svg>
}

export default LineGraphFitness
