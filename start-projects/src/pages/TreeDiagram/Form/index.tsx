// PLUGINS IMPORTS //
import React, { useState, FormEvent, FC } from "react"
import { db } from "api"
import constants from "api/constants"

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface PropsType {
  setEditing: (isEditing: boolean) => void
}

type IField = "name" | "parent" | "departament"
const initState = { name: "", parent: "", departament: "" }
const Form: FC<PropsType> = (props) => {
  const [state, setState] = useState(initState)

  const handleChange = (e: any, value: IField) =>
    setState({ ...state, [value]: e.target.value })

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault()
    db.collection(constants.employees).add(state)
    props.setEditing(false)
  }

  return (
    <div style={{ alignSelf: "center", width: "50%" }}>
      <div className="modal-content">
        <h4 className="pink-text center">Add a New Employee</h4>
        <form onSubmit={handleSubmit}>
          <div className="input-field">
            <input
              type="text"
              id="name"
              placeholder="Employee name"
              value={state.name}
              onChange={(e) => handleChange(e, "name")}
            />
          </div>
          <div className="input-field">
            <input
              type="text"
              id="parent"
              placeholder="Reports to..."
              value={state.parent}
              onChange={(e) => handleChange(e, "parent")}
            />
          </div>
          <div className="input-field">
            <input
              type="text"
              id="department"
              placeholder="Department"
              value={state.departament}
              onChange={(e) => handleChange(e, "departament")}
            />
          </div>
          <div className="input-field center">
            <button className="btn pink white-text">Add Employee</button>
          </div>
        </form>
      </div>
    </div>
  )
}

const styles = {
  wrapper: {},
}

export default Form
