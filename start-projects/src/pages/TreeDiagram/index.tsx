// PLUGINS IMPORTS //
import React, { useState } from "react"

// COMPONENTS IMPORTS //
import Form from "./Form"
import Graph from "./Graph"

// EXTRA IMPORTS //
import "./index.css"

/////////////////////////////////////////////////////////////////////////////

const TreeDiagram = () => {
  const [editing, setEditing] = useState<boolean>(false)

  return (
    <div style={{ flex: 1, display: "flex", flexDirection: "column" }}>
      <div className="pink section">
        <h2 className="white-text center">Ninja Corp</h2>
      </div>
      <div
        className="grey lighten-3 section grey-text"
        style={{ position: "relative" }}
      >
        <p className="flow-text center">The Number ONE Ninja Company</p>
        <a
          className="btn-floating btn-large halfway-fab pink modal-trigger"
          href="#modal"
          onClick={() => setEditing((prev) => !prev)}
        >
          <i className="material-icons">add</i>
        </a>
      </div>

      {editing ? <Form setEditing={setEditing} /> : <Graph />}
    </div>
  )
}

export default TreeDiagram
