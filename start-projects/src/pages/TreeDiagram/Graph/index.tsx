// PLUGINS IMPORTS //
import React, { useEffect } from "react"
import * as d3 from "d3"
import { db } from "api"
import constants from "api/constants"

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IDataItem {
  id: string
  name: string
  parent: string
  departament: string
}

const Graph = () => {
  useEffect(() => {
    generateGraph()
  }, [])

  // # Constants //
  const margin = 100
  const dimensions = { height: 500, width: 1110 }
  function generateGraph() {
    // # Setup //
    const svg = d3.select("svg")
    const GGraph = svg.append("g").attr("transform", "translate(50, 50)")

    // # Data stratify
    const stratify = d3
      .stratify()
      .id((d: any) => d.name)
      .parentId((d: any) => d.parent)

    const tree = d3.tree().size([dimensions.width, dimensions.height])

    // create color (ordinal scale)
    const color = d3.scaleOrdinal(["#f4511e", "#e91e63", "#e53935", "#9c27b0"])

    // # Update function //
    function update(data: Array<IDataItem>) {
      // remove current nodes
      GGraph.selectAll("g").remove()
      GGraph.selectAll("path").remove()

      // update color (ordinal scale)
      color.domain(data.map((item) => item.departament))

      // 1. get updated root node data
      const rootNode = stratify(data)
      const treeData = tree(rootNode)

      // get nodes selection & join data
      const nodes = GGraph.selectAll("g").data(treeData.descendants())

      // get link selection and join data
      const links = GGraph.selectAll("path").data(treeData.links())

      // enter new links
      links
        .enter()
        .append("path")
        .attr("fill", "none")
        .attr("stroke", "#aaa")
        .attr("stroke-width", 2)
        .attr(
          "d",
          d3
            .linkVertical()
            .x((d: any) => d.x)
            .y((d: any) => d.y) as any
        )

      // create enter node groups
      const enterNodes = nodes
        .enter()
        .append("g")
        .attr("transform", (d) => `translate(${d.x}, ${d.y})`)

      // append elements (shapes) to nodes
      enterNodes
        .append("rect")
        .attr("fill", (d: any) => color(d.data.departament))
        .attr("stroke", "#555")
        .attr("stroke-width", 2)
        .attr("height", 50)
        .attr("width", (d: any) => d.data.name.length * 20)
        .attr("transform", (d: any) => {
          const x = d.data.name.length * 10
          return `translate(${-x}, -25)`
        })

      // append text
      enterNodes
        .append("text")
        .attr("text-anchor", "middle")
        .attr("fill", "white")
        .text((d: any) => d.data.name)
    }

    const fetchData = () => {
      let data: Array<IDataItem> = []

      db.collection(constants.employees).onSnapshot((res) => {
        res.docChanges().forEach((change) => {
          const doc = {
            id: change.doc.id,
            ...change.doc.data(),
          } as IDataItem

          switch (change.type) {
            case "added":
              data.push(doc)
              break
            case "modified":
              const index = data.findIndex((el) => el.id === doc.id)
              data[index] = doc
              break
            case "removed":
              data = data.filter((el) => el.id !== doc.id)
              break
            default:
              break
          }
        })

        update(data)
      })
    }

    fetchData()
  }
  return (
    <div className={"container"}>
      <svg
        height={dimensions.height + margin}
        width={dimensions.width + margin}
      ></svg>
    </div>
  )
}

export default Graph
