// PLUGINS IMPORTS //
import React, { useEffect } from "react"
import * as d3 from "d3"
import { db } from "api"
import constants from "api/constants"

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const BarChart = () => {
  useEffect(() => {
    renderD3()
  }, [])

  // # Constants // ----------------->
  const SIZE = 600
  const margin = { top: 20, right: 20, bottom: 100, left: 100 }
  const graphHeight = SIZE - margin.top - margin.bottom
  const graphWidth = SIZE - margin.left - margin.right

  const renderD3 = async () => {
    // # Setup // ----------------->
    const svg = d3
      .select(".canvas")
      .append("svg")
      .attr("width", SIZE)
      .attr("height", SIZE)

    const GGraph = svg
      .append("g")
      .attr("height", graphHeight)
      .attr("width", graphWidth)
      .attr("transform", `translate(${margin.left},${margin.top})`)

    const GxAxis = GGraph.append("g").attr(
      "transform",
      `translate(0, ${graphHeight})`
    )
    const GyAxis = GGraph.append("g")

    // scales
    const y = d3.scaleLinear().range([graphHeight, 0])
    const x = d3.scaleBand().range([0, 500]).paddingInner(0.2).paddingOuter(0.2)

    // create the axis
    const xAxis = d3.axisBottom(x)
    const yAxis = d3
      .axisLeft(y)
      .ticks(10)
      .tickFormat((d) => `${d} orders`)

    // formating text (a-axis)
    GxAxis.selectAll("text")
      .attr("transform", `rotate(-40)`)
      .attr("text-anchor", "end")

    // # Update Function // ----------------->
    const update = (data: Array<any>) => {
      const t = d3.transition().duration(1500) as any
      const names = data.map((el) => el.name)
      const max = d3.max(data, (d) => d.orders) as number

      // 1. join the data to rects
      const rects = GGraph.selectAll("rect").data(data)

      // 2. updating scale (domains) if they relay on our data
      y.domain([0, max])
      x.domain(names)

      // 3. remove unwanted (if any) shapes using selection
      rects.exit().remove()

      // 4. update current shapes in DOM
      rects
        .attr("fill", "orange")
        .attr("x", (d) => {
          const value = x(d.name)
          return value !== undefined && value
        })
        .transition(t)
        .attr("height", (d) => graphHeight - y(d.orders))
        .attr("y", (d) => y(d.orders))

      // 5. append the enter selection to the DOM
      rects
        .enter()
        .append("rect")
        .attr("height", 0)
        .attr("y", graphHeight)
        .attr("fill", "orange")
        .attr("x", (d) => {
          const value = x(d.name)
          return value !== undefined && value
        })
        .transition(t)
        .attrTween("width", widthTween as any)
        .attr("height", (d) => graphHeight - y(d.orders))
        .attr("y", (d) => y(d.orders))

      // 6. call axis
      GxAxis.call(xAxis)
      GyAxis.call(yAxis)
    }

    // TWEENS
    const widthTween = (d: any) => {
      // define interpolation
      // d3.interpolate returns a function which we call 'i'
      let i = d3.interpolate(0, x.bandwidth())

      // return a function which takes in a time ticker 't'
      return (t: any) => {
        // return the value from passing the ticker into the interpolation
        return i(t)
      }
    }

    var data: Array<any> = []
    db.collection(constants.dishes).onSnapshot((res) => {
      console.log(res)
      res.docChanges().forEach((change) => {
        const doc = { ...change.doc.data(), id: change.doc.id }
        console.log(doc)
        switch (change.type) {
          case "added":
            data.push(doc)
            break
          case "modified":
            const index = data.findIndex((el) => el.id === doc.id)
            data[index] = doc
            break
          case "removed":
            data = data.filter((el) => el.id !== doc.id)
            break
          default:
            break
        }
      })

      update(data)
    })
  }

  return <div className={"canvas"}></div>
}

export default BarChart
