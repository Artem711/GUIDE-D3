// PLUGINS IMPORTS //
import React from "react"

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const Header = () => {
  return (
    <header className="indigo darken-1 section" style={styles.wrapper}>
      <h2 className="center white-text">Ninja Wonga</h2>
      <p className="flow-text grey-text center text-lighten-2">
        Monthly money tracker for ninjas
      </p>
    </header>
  )
}

const styles = {
  wrapper: { marginBottom: 30, paddingBottom: 10 },
}

export default Header
