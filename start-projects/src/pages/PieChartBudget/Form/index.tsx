// PLUGINS IMPORTS //
import React, { useState, FormEvent } from "react"
import { db } from "../../../api"

// COMPONENTS IMPORTS //
import InputItem from "./InputItem/InputItem"
import Button from "./Button/Button"

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const Form = () => {
  const initState = { name: null as string | null, cost: 0 }
  const [state, setState] = useState(initState)

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault()
    db.collection("expenses")
      .add(state)
      .then(() => setState(initState))
  }

  return (
    <form className="card z-depth-0" action={"submit"}>
      <div className="card-content">
        <span className="card-title indigo-text">Add Item:</span>
        <InputItem
          label={"Item Name"}
          value={state.name}
          onChage={(text) => setState({ ...state, name: text })}
        />
        <InputItem
          label={"Item Cost ($)"}
          value={state.cost}
          onChage={(text) => setState({ ...state, cost: parseFloat(text) })}
        />
        <Button onClick={handleSubmit}>Add Item</Button>

        <div className={"input-field center"}>
          <p className="red-text">test</p>
        </div>
      </div>
    </form>
  )
}

export default Form
