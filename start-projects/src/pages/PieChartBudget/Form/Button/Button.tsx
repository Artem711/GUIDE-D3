// PLUGINS IMPORTS //
import React, { FC, ReactNode } from "react"

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface PropsType {
  children?: ReactNode
  onClick?: (event?: any) => void
}

const Button: FC<PropsType> = (props) => {
  return (
    <div className="input-field center">
      <button onClick={props.onClick} className="btn-large pink white-text">
        {props.children}
      </button>
    </div>
  )
}

export default Button
