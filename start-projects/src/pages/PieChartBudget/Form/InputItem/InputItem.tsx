// PLUGINS IMPORTS //
import React, { FC } from "react"

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface PropsType {
  label: string
  value?: string | number | null
  onChage?: (text: string) => void
}

const InputItem: FC<PropsType> = (props) => {
  return (
    <div className="input-field">
      <input
        type="text"
        id={"name"}
        value={props.value || ""}
        onChange={(event) => props.onChage && props.onChage(event.target.value)}
      />
      <label htmlFor="name">{props.label}</label>
    </div>
  )
}

export default InputItem
