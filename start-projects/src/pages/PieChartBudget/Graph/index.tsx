// PLUGINS IMPORTS //
import React, { useEffect } from "react"
import * as d3 from "d3"
import { legendColor } from "d3-svg-legend"
import constants from "api/constants"
import { db } from "api"

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IDataItem {
  id: string
  name: string
  cost: number
}
const Graph = () => {
  useEffect(() => {
    generateGraph()
  }, [generateGraph])

  // # Constants //
  const size = 300
  const padding = 5
  const margin = 150
  const dims = { height: size, width: size, radius: size / 2 }
  const center = { x: dims.width / 2 + padding, y: dims.height / 2 + padding }

  function generateGraph() {
    // # Setup //
    const svg = d3.select("svg")
    const GGraph = svg
      .append("g")
      .attr("transform", `translate(${center.x}, ${center.y})`)

    const generateAngles = d3 // value we evaluate to create the pie angles
      .pie()
      .sort(null)
      .value((d: any) => d.cost)

    const generateArcPath = d3 // returns: a path based on entered angles
      .arc()
      .outerRadius(dims.radius)
      .innerRadius(dims.radius / 2)

    const color = d3.scaleOrdinal(d3["schemeSet3"])

    // legend setup
    const GLegend = svg
      .append("g")
      .attr("transform", `translate(${dims.width + 40}, 10)`)

    const legend = legendColor().shape("circle").shapePadding(10).scale(color)

    // # Update function //
    const update = (data: Array<IDataItem>) => {
      // 1. generate values based on incoming data
      const angles = generateAngles(data as any)
      const paths = GGraph.selectAll("path").data(angles)
      color.domain(data.map((el) => el.name))

      // 2. update & call legend
      GLegend.call(legend as any)
      GLegend.selectAll("text").attr("fill", "white")

      // 3. handle exit selection
      paths
        .exit()
        .transition()
        .duration(750)
        .attrTween("d", (d: any) => arcInterpolateTween(d, true) as any)
        .remove()

      // 4. handle current DOM updates
      paths
        .attr("d", generateArcPath as any)
        .transition()
        .duration(750)
        .attrTween("d", arcUpdateTween as any)

      // 5. handle enter new elements
      paths
        .enter()
        .append("path")
        .attr("class", "arc")
        .attr("stroke", "white")
        .attr("stroke-width", 3)
        .attr("fill", (d: any) => color(d.data.name))
        .each(function (d) {
          // @ts-ignore
          this._current = d
        })
        .transition()
        .duration(750)
        .attrTween("d", (d) => arcInterpolateTween(d) as any)

      // 6. add events
      GGraph.selectAll("path")
        .on("mouseover", handleMouseOver)
        .on("mouseout", handleMouseOut)
        .on("click", handleClick)
    }

    // # Fetch data from database
    const fetchData = () => {
      // data array (from firestore)
      let data: Array<IDataItem> = []
      db.collection(constants.expenses).onSnapshot((res) => {
        res.docChanges().forEach((change) => {
          const doc = { id: change.doc.id, ...change.doc.data() } as IDataItem
          switch (change.type) {
            case "added":
              data.push(doc)
              break
            case "modified":
              const index = data.findIndex((el) => el.id === doc.id)
              data[index] = doc
              break
            case "removed":
              data = data.filter((el) => el.id !== doc.id)
              break
            default:
              break
          }
        })

        update(data)
      })
    }
    fetchData()

    // # Tweens //
    const arcInterpolateTween = (
      d: { startAngle: number; endAngle: number },
      isExit?: boolean
    ) => {
      let i = isExit
        ? d3.interpolate(d.startAngle, d.endAngle)
        : d3.interpolate(d.endAngle, d.startAngle)

      return (t: any) => {
        d.startAngle = i(t)
        return generateArcPath(d as any)
      }
    }

    function arcUpdateTween(this: { _current: any }, d: any) {
      // interpolate btw the two objects
      let i = d3.interpolate(this._current, d)

      // update the current prop with updated data
      this._current = i(1)
      return (t: any) => generateArcPath(i(t))
    }

    // # Event handlers //
    function handleMouseOver(this: any, d: any, i: any) {
      d3.select(this)
        .transition("mousehover")
        .duration(300)
        .attr("fill", "white")
    }

    function handleMouseOut(this: any, d: any, i: any) {
      d3.select(this)
        .transition("mousehover")
        .duration(300)
        .attr("fill", color(i.data.name))
    }

    function handleClick(this: any, d: any, i: any) {
      db.collection(constants.expenses).doc(i.data.id).delete()
    }
  }

  return (
    <svg
      className={"canvas"}
      height={dims.height + margin}
      width={dims.width + margin}
    ></svg>
  )
}

export default Graph
