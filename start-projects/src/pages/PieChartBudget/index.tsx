// PLUGINS IMPORTS //
import React from "react"

// COMPONENTS IMPORTS //
import Header from "./Header/Header"
import Form from "./Form"
import Graph from "./Graph"

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const PieChartBudget = () => {
  return (
    <div className={"indigo"}>
      <Header />

      <div className="container section">
        <div className="row">
          <div className="col s12 m6">
            <Form />
          </div>
          <div className="col s12 m5 push-m1">
            <Graph />
          </div>
        </div>
      </div>
    </div>
  )
}

export default PieChartBudget
