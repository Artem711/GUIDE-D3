// PLUGINS IMPORTS //
import React, { useEffect, FC } from "react"
import { db } from "api"
import constants from "api/constants"
import * as d3 from "d3"

// COMPONENTS IMPORTS //

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

interface IDataItem {
  id: string
  activity: string
  distance: number
  date: string
}

interface PropsType {
  activity: string
}

const Graph: FC<PropsType> = (props) => {
  useEffect(() => {
    generateGraph()
  }, [props.activity, generateGraph])

  // # Constants //
  const margin = { top: 40, right: 20, bottom: 50, left: 100 }
  const graphWidth = 560 - margin.left - margin.right
  const graphHeight = 400 - margin.top - margin.bottom

  function generateGraph() {
    // # Setup //
    const svg = d3.select("svg")

    const GGraph = svg
      .append("g")
      .attr("width", graphWidth)
      .attr("height", graphHeight)
      .attr("transform", `translate(${margin.left}, ${margin.top})`)

    // # Scales //
    const x = d3.scaleTime().range([0, graphWidth])
    const y = d3.scaleLinear().range([graphHeight, 0])

    // # Axis groups //
    const GxAxis = GGraph.append("g")
      .attr("class", "x-axis")
      .attr("transform", `translate(0, ${graphHeight})`)
    const GyAxis = GGraph.append("g").attr("class", "y-axis")

    // # D3 Line path generator
    const line = d3
      .line()
      .x(function (d: any) {
        return x(new Date(d.date))
      })
      .y(function (d: any) {
        return y(d.distance)
      }) as any

    // append line path
    const path = GGraph.append("path")

    // dotted line group and append to graph
    const dottedLines = GGraph.append("g")
      .attr("class", "lines")
      .style("opacity", 0)

    const strokeConfig = {
      color: "#aaa",
      width: 1,
      dasharray: 4,
    }

    // create 'x' dotted line and apend to dotted line group
    const xDottedLine = dottedLines
      .append("line")
      .attr("stroke", strokeConfig.color)
      .attr("stroke-width", strokeConfig.width)
      .attr("stroke-dasharray", strokeConfig.dasharray)

    // create 'y' dotted line and apend to dotted line group
    const yDottedLine = dottedLines
      .append("line")
      .attr("stroke", strokeConfig.color)
      .attr("stroke-width", strokeConfig.width)
      .attr("stroke-dasharray", strokeConfig.dasharray)

    // # Update function //
    const update = (data: Array<IDataItem>) => {
      // filter data (based on activity)
      data = data.filter((el) => el.activity === props.activity)
      // sort data (based on date)
      data = data.sort(
        (a, b) => (new Date(a.date) as any) - (new Date(b.date) as any)
      )

      // 1. set scale domains
      x.domain(d3.extent(data, (d) => new Date(d.date)) as any)
      y.domain([0, d3.max(data, (d) => d.distance) as number])

      // update path data
      path
        .data([data])
        .attr("fill", "none")
        .attr("stroke", "#00bfa5")
        .attr("stroke-width", 2)
        .attr("d", line)

      // 2. create circles for points
      const circles = GGraph.selectAll("circle").data(data)

      // 3. remove unwanted points
      circles.exit().remove()

      // 4. update current points
      circles
        .attr("cx", (d) => x(new Date(d.date)))
        .attr("cy", (d) => y(d.distance))

      // 5. add new points
      circles
        .enter()
        .append("circle")
        .attr("r", 4)
        .attr("cx", (d) => x(new Date(d.date)))
        .attr("cy", (d) => y(d.distance))
        .attr("fill", "#ccc")

      // 6. hover effects
      GGraph.selectAll("circle")
        .on("mouseover", handleHover)
        .on("mouseleave", handleLeave)

      // 6. create axis
      const xAxis = d3
        .axisBottom(x)
        .ticks(4)
        .tickFormat(d3.timeFormat("%b %d") as any)
      const yAxis = d3
        .axisLeft(y)
        .ticks(4)
        .tickFormat((text) => `${text}m`)

      // 7. call axis
      GxAxis.call(xAxis)
      GyAxis.call(yAxis)

      // rotate axis text
      GxAxis.selectAll("text")
        .attr("text-anchor", "end")
        .attr("transform", `rotate(-40)`)
    }

    let data: Array<IDataItem> = []
    // # Fetch data from database
    db.collection(constants.activity)
      // .where("activity", "==", props.activity)
      .orderBy("date")
      .onSnapshot((res) => {
        res.docChanges().forEach((change) => {
          const doc = {
            id: change.doc.id,
            ...change.doc.data(),
          } as IDataItem

          switch (change.type) {
            case "added":
              data.push(doc)
              break
            case "modified":
              const index = data.findIndex((el) => el.id === doc.id)
              data[index] = doc
              break
            case "removed":
              data = data.filter((el) => el.id !== doc.id)
              break
            default:
              break
          }
        })

        update(data)
      })

    function handleHover(this: any, _: any, d: any) {
      d3.select(this)
        .transition()
        .duration(100)
        .attr("r", 8)
        .attr("fill", "white")

      // set x dotted line coords (x1, x2, y1, y2)
      xDottedLine
        .attr("x1", x(new Date(d.date)))
        .attr("x2", x(new Date(d.date)))
        .attr("y1", graphHeight)
        .attr("y2", y(d.distance))

      // set y dotted line coords (x1, x2, y1, y2)
      yDottedLine
        .attr("x1", 0)
        .attr("x2", x(new Date(d.date)))
        .attr("y1", y(d.distance))
        .attr("y2", y(d.distance))

      // show the dotted line group (.style, opacity)
      dottedLines.style("opacity", 1)
    }

    function handleLeave(this: any, _: any, d: any) {
      d3.select(this)
        .transition()
        .duration(100)
        .attr("r", 4)
        .attr("fill", "#ccc")

      dottedLines.style("opacity", 0)
    }
  }

  return (
    <svg
      width={graphWidth + margin.left + margin.right}
      height={graphHeight + margin.top + margin.bottom}
    ></svg>
  )
}

export default Graph
