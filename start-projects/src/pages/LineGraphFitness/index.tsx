// PLUGINS IMPORTS //
import React, { useState, FormEvent } from "react"
import { db } from "api"
import constants from "api/constants"

// COMPONENTS IMPORTS //
import Graph from "./Graph"

// EXTRA IMPORTS //
import "./index.css"

/////////////////////////////////////////////////////////////////////////////

type ISelector = "Cycling" | "Running" | "Swimming" | "Walking"
const buttons: Array<ISelector> = ["Cycling", "Running", "Swimming", "Walking"]

const LineGraphFitness = () => {
  const [activity, setActivity] = useState<ISelector>("Swimming")
  const [distance, setDistance] = useState(0)

  const handleChange = (number: number) => setDistance(number)
  const handleSubmit = (e: FormEvent) => {
    e.preventDefault()
    db.collection(constants.activity).add({
      activity,
      distance,
      date: new Date().toString(),
    })
    setDistance(0)
  }

  return (
    <body className="grey darken-4">
      <div className="nav z-depth-0">
        <div className="section center">
          <h3 className="white-text center">- The Dojo -</h3>
        </div>
      </div>

      <div className="section center">
        <p className="grey-text flow-text">A fitness tracker for ninjas</p>
      </div>

      <div className="container section">
        <div className="row">
          <div className="col s12 l5">
            {buttons.map((el) => (
              <button
                className={el === activity ? "active" : undefined}
                onClick={() => setActivity(el)}
              >
                {el}
              </button>
            ))}
          </div>
          <div className="col s12 l6 push-l1">
            <Graph activity={activity} />
          </div>
        </div>
        <div className="row">
          <form className="col m6 push-m3" onSubmit={handleSubmit}>
            <p className="flow-text grey-text center">
              How much <span>{activity}</span> have you done today?
            </p>
            <input
              className="grey-text"
              type="text"
              id="cycling"
              placeholder="Distance in km"
              onChange={(e) => {
                const value = parseFloat(e.target.value)
                isNaN(value) ? handleChange(0) : handleChange(value)
              }}
              value={distance}
            />
            <p className="center pink-text error text-lighten-1"></p>
          </form>
        </div>
      </div>
    </body>
  )
}

export default LineGraphFitness
