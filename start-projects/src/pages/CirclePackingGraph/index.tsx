// PLUGINS IMPORTS //
import React, { useEffect } from "react"
import * as d3 from "d3"

// COMPONENTS IMPORTS //
import data from "./data"

// EXTRA IMPORTS //

/////////////////////////////////////////////////////////////////////////////

const LineGraphFitness = () => {
  useEffect(() => {
    generateGraph()
  }, [])

  function generateGraph() {
    // create svg
    const svg = d3.select("svg")
    // create graph
    const GGraph = svg.append("g").attr("transform", "translate(50, 50)")

    const stratify = d3
      .stratify()
      .id((d: any) => d.name)
      .parentId((d: any) => d.parent)

    const rootNode = stratify(data).sum((d: any) => d.amount)
    const pack = d3.pack().size([960, 700]).padding(5)
    const result = pack(rootNode).descendants()

    // ordinal scale
    const color = d3.scaleOrdinal(["#d1c4e9", "#b39ddb", "#9575cd"])

    // join data and add nodes
    const nodes = GGraph.selectAll("g")
      .data(result)
      .enter()
      .append("g")
      .attr("transform", (d) => `translate(${d.x}, ${d.y})`)

    nodes
      .append("circle")
      .attr("r", (d) => d.r)
      .attr("stroke", "white")
      .attr("stroke-width", 2)
      .attr("fill", (d) => color(String(d.depth)))

    nodes
      .filter((d) => !d.children)
      .append("text")
      .attr("text-anchor", "middle")
      .attr("dy", "0.3em")
      .attr("fill", "white")
      .style("font-size", (d) => (d.value || 4) * 5)
      .text((d: any) => d.data.name)
  }
  return <svg height={800} width={1060}></svg>
}

export default LineGraphFitness
